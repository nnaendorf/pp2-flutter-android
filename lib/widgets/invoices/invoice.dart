import 'dart:io';

class Invoice {
  String title;
  String description;
  double price;
  File image;

  Invoice({
    this.title,
    this.description,
    this.price,
    this.image,
  });
}