import 'package:flutter/material.dart';

// Changenotifier => State Management Object
class User with ChangeNotifier {
  String _session = '';

  String get session {
    return _session;
  }

  void setSession(String session) {
    this._session = session;
  }
}